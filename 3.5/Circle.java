public class Circle {
    public double radius;
    public String color;
    Circle(){
        radius=1.0;
        color="red";
    }
    Circle(double r,String c){
        radius=r;
        color=c;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return radius*radius*Math.PI;
    }

    public static void main(String[] args) {
        Circle c1=new Circle();
        System.out.println("the circles radius is:"+c1.getRadius()+" and the color is "+c1.color+" and the area is :"+c1.getArea());

        Circle c2=new Circle(3.0,"blue");
        System.out.println("the circles radius is:"+c2.getRadius()+" and the color is: "+c2.color+" and the area is :"+c2.getArea());
    }
}
